================= Spring JDBC ========================================================================
1. Cấu hình
  - Tạo 1 lớp chứa thông tin cấu hình  
  - Đặt tên Repository hoặc DAO (MVC)
    + Tạo đối tượng DriverManagerDataSource
	+ Tạo đối tượng JDBCTemplate
2. Thực thi các lệnh liên quan đến dữ liệu -> sử dụng các hàm của đối tượng JDBCTemplate:
  - Lấy danh sách đối tượng: query([parameter])
  - Lấy về 1 đối tượng: queryForObject([parameter])
  => Chuyển đổi kết quả từ result -> đối tượng
  => Định nghĩa lớp mapping 
  
  - tác vụ insert, update và delete: hàm update()
  