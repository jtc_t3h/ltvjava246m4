<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<h2>Chapter9: Spring Security</h2>
<li>Welcome <span>${pageContext.request.userPrincipal.name}</span>
  <form class="form-hidden" method="post" action="${pageContext.request.contextPath}/chapter9/auth/bai4_logoff.html">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    <button>Log Off</button>
  </form>
</li>