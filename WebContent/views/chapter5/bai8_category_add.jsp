<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="page-header">Add Category</div>
<form method="post" class="form">
	<p>
		<label>Name</label>
		<input type="text" name="name" />
	</p>
	<p>
		<label>Parent</label>
		<select name="parentId">
			<option value="">--Select Parent--</option>
			<c:forEach items="${list }" var="obj">
			  <option value="${obj.id }">${obj.name }</option>
			</c:forEach>
		</select>
	</p>
	<p>
		<button class="btn btn-primary">Save</button>
	</p>
</form>