<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<a href="${pageContext.request.contextPath}/admin/member/add.html"
		class="btn btnprimary">Add</a>
	<form method="post"
		action="${pageContext.request.contextPath}/admin/member/dels.html">
		<table class="table">
			<tr>
				<td><button class="btn btn-info">Delete</button></td>
				<th>Id</th>
				<th>Name</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
			<c:forEach items="${list}" var="o">
				<tr>
					<td><input type="checkbox" value="${o.id}" name="ids">
					</td>
					<td>${o.id}</td>
					<td>${o.username}</td>
					<td>${o.email}</td>
					<td>${o.gender}</td>
					<td>${o.tel}</td>
					<td>${o.address}</td>
					<td>${o.addeddate}</td>
					
					<td><a
						href="${pageContext.request.contextPath}/admin/member/edit.html/${o.id}">
							<img src="${pageContext.request.contextPath}/images/edit.png"
							alt="Edit">
					</a></td>
					<td><a
						href="${pageContext.request.contextPath}/admin/member/del.html/${o.id}">
							<img src="${pageContext.request.contextPath}/images/trash.png"
							alt="Delete">
					</a></td>
				</tr>
			</c:forEach>
		</table>
	</form>
</body>
</html>