<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<form:form method="post" modelAttribute="obj" cssClass="form">
<p>
		<label>User name</label>
		<form:input path="username" />
	</p>
	<p>
		<label>Password</label>
		<form:input path="password" />
	</p>
	<p>
		<label>Email</label>
		<form:input path="email" />
	</p>
<%-- 	<p>
		<label>Gender</label>
		<form:select path="gender">
			<form:option value="-1">--Select--</form:option>
			<form:options items="${map}" />
		</form:select>
	</p> --%>
	<p>
		<label>Tel</label>
		<form:input path="tel" />
	</p>
	<p>
		<label>Address</label>
		<form:input path="address" />
	</p>
  <p>
    <button>Save</button>
  </p>
</form:form>
