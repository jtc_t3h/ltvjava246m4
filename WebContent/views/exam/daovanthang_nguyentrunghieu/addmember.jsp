<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="page-header">Add Member</div>
<form:form method="post" modelAttribute="Member" cssClass="form">
	<p>
		<label>User name</label>
		<form:input path="username" />
	</p>
	<p>
		<label>Password</label>
		<form:input path="password" />
	</p>
	<p>
		<label>Email</label>
		<form:input path="email" />
	</p>
	<p>
		<label>Gender</label>
		<form:select path="gender">
			<form:option value="-1">--Select--</form:option>
			<form:options items="${map}" />
		</form:select>
	</p>
	<p>
		<label>Tel</label>
		<form:input path="tel" />
	</p>
	<p>
		<label>Address</label>
		<form:input path="address" />
	</p>
	<p>
		<button class="btn btn-primary">Save</button>
	</p>
</form:form>
