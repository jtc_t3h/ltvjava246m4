<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="page-header">List Category</div>
<a href="${pageContext.request.contextPath}/exam/daovanthang/member/add.html"
	class="btn btn-primary">Add</a>
<form method="post" action="${pageContext.request.contextPath}/daovanthang/member/dels.html">
<table class="table table-bordered">
	<tr>
		<td><button class="btn btn-info">Delete</button> </td>
		<th>UserName</th>
		<th>Email</th>
		<th>Gender</th>
		<th>Tel</th>
		<th>Address</th>
		<th>AddressDate</th>
		<th><a href="">Edit</a></th>
		<th><a href="">Delete</a></th>
	</tr>
	<c:forEach items="${list}" var="o">
		<tr>
			<td>
		<input type="checkbox" value="${o.id}" name="ids"></td>

			<td>${o.username}</td>
			<td>${o.email}</td>
			<td>${o.gender}</td>
			<td>${o.tel}</td>
			<td>${o.address}</td>
			<td>${o.addedDate}</td>
			<td><a
				href="${pageContext.request.contextPath}/exam/daovanthang/member/edit.html/${o.id}">
					<img src="${pageContext.request.contextPath}/images/edit.png"
					alt="Edit">
			</a></td>
			<td><a
				href="${pageContext.request.contextPath}/exam/daovanthang/member/del.html/${o.id}">
					<img src="${pageContext.request.contextPath}/images/trash.png"
					alt="Delete">
			</a></td>
		</tr>
	</c:forEach>
</table>
</form>