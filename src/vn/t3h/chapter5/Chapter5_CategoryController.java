package vn.t3h.chapter5;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.chapter5.dao.CategoryRepository;
import vn.t3h.chapter5.domain.Category;

@Controller
@RequestMapping("/chapter5/category/")
public class Chapter5_CategoryController {
	
	private CategoryRepository repository = new CategoryRepository();
	
	@RequestMapping("list.html")
	public String list(Model model) {
		model.addAttribute("list", repository.getCategories());
		return "chapter5.category.list";
	}

	@RequestMapping("add.html")
	public String add(Model model) {
		model.addAttribute("list", repository.getCategories());
		return "chapter5.category.add";
	}

	@RequestMapping(value = "add.html", method = RequestMethod.POST)
	public String add(Category obj) {
		repository.add(obj);
		return "redirect:/chapter5/category/list.html";
	}
}
