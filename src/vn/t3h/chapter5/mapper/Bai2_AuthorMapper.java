package vn.t3h.chapter5.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import vn.t3h.chapter5.domain.Bai2_Author;

public class Bai2_AuthorMapper implements RowMapper<Bai2_Author> {

	@Override
	public Bai2_Author mapRow(ResultSet rs, int arg1) throws SQLException {
		Bai2_Author author = new Bai2_Author();
		author.setId(rs.getInt(1));
		author.setName(rs.getString(2));
		return author;
	}

}
