package vn.t3h.chapter5.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import vn.t3h.chapter5.domain.Category;

public class CategoryMapper implements RowMapper<Category> {

	@Override
	public Category mapRow(ResultSet rs, int arg1) throws SQLException {
		return new Category(rs.getInt(1), rs.getString(2), rs.getInt(3));
	}

}
