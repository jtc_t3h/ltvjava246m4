package vn.t3h.chapter5.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class AbstractDAO {

	protected static JdbcTemplate jdbcTemplate;
	
	static {
		if (jdbcTemplate == null) {
			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName("");
			dataSource.setUrl("");
			dataSource.setUsername("");
			dataSource.setPassword("");
			
			jdbcTemplate = new JdbcTemplate(dataSource);
		}
	}
	
}
