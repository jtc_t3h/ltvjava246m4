package vn.t3h.chapter5.dao;

import java.util.List;

import vn.t3h.chapter5.domain.Bai2_Author;
import vn.t3h.chapter5.mapper.Bai2_AuthorMapper;

public class Bai2_AuthorRepository extends Repository {
	// Lien quan den cac thao tac xuong table => 4 tac vu

	public List<Bai2_Author> getAuthors() {
		return jdbc.query("select * from author", new Bai2_AuthorMapper());
	}

	public int add(Bai2_Author obj) {
		return jdbc.update("INSERT INTO Author (AuthorName) VALUES(?)", obj.getName());
	}

	public int edit(Bai2_Author obj) {
		return jdbc.update("UPDATE Author SET AuthorName = ? WHERE AuthorId = ?", obj.getName(), obj.getId());
	}

	public Bai2_Author getAuthor(int id) {
		return jdbc.queryForObject("SELECT * FROM Author WHERE AuthorId = ?", new Bai2_AuthorMapper(), id);
	}

	public int delete(int id) {
		return jdbc.update("DELETE FROM Author WHERE AuthorId = ?", id);
	}

	public int delete(List<Integer> list) {
		for (Integer id : list) {
			jdbc.update("DELETE FROM Author WHERE AuthorId = ?", id);
		}
		return 1;
	}
}
