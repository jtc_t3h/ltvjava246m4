package vn.t3h.chapter5.dao;

import java.util.List;

import vn.t3h.chapter5.domain.Author;
import vn.t3h.chapter5.mapper.AuthorMapper;

public class AuthorDAO extends AbstractDAO {

	public List<Author> list(){
		return jdbcTemplate.query("select * from author", new AuthorMapper());
	}
	
	public Author findById(Long id) {
		return jdbcTemplate.queryForObject("select * from author where id = " + id, new AuthorMapper());
	}
}
