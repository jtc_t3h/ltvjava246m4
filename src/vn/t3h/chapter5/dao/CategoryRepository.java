package vn.t3h.chapter5.dao;

import java.util.List;

import vn.t3h.chapter5.domain.Category;
import vn.t3h.chapter5.mapper.CategoryMapper;

public class CategoryRepository extends Repository{

	public List<Category> getCategories(){
		return jdbc.query("select * from category", new CategoryMapper());
	}
	
	public int add(Category obj) {
		return jdbc.update("INSERT INTO category (categoryName, parentId) VALUES(?,?)", 
				obj.getName(), obj.getParentId());
	}
	
	public Category getCategory(int id){
		return jdbc.queryForObject("SELECT * FROM Category WHERE CategoryId = ?", new CategoryMapper(), id);
	}
	
	public int edit(Category obj) {
		return jdbc.update("UPDATE Category SET CategoryName = ? , ParentId = ? WHERE CategoryId = ?", 
				obj.getName(), obj.getParentId(), obj.getId()); 
				
	}
}
