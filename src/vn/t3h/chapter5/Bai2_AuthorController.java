package vn.t3h.chapter5;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import vn.t3h.chapter5.dao.Bai2_AuthorRepository;
import vn.t3h.chapter5.domain.Bai2_Author;

@Controller
public class Bai2_AuthorController {

	private Bai2_AuthorRepository repository = new Bai2_AuthorRepository();

	@RequestMapping("/chapter5/author/list.html")
	public String index(Model model) {
		List<Bai2_Author> list = repository.getAuthors();
		System.out.println(list.size());

		model.addAttribute("list", list);
		return "chapter5.author.list";
	}

	@RequestMapping("/chapter5/author/add.html")
	public String add() {
		return "chapter5.author.add";
	}

	@RequestMapping(value = "/chapter5/author/add.html", method = RequestMethod.POST)
	public String add(Bai2_Author obj) {
		repository.add(obj);
		return "redirect:/chapter5/author/list.html";
	}

	@RequestMapping("/chapter5/author/edit.html/{id}")
	public String edit(Model model, @PathVariable("id") int id) {
		model.addAttribute("o", repository.getAuthor(id));
		return "chapter5.author.edit";
	}

	@RequestMapping(value = "/chapter5/author/edit.html/{id}", method = RequestMethod.POST)
	public String edit(Model model, Bai2_Author obj, @PathVariable("id") int id) {
		repository.edit(obj);
		return "redirect:/chapter5/author/list.html";
	}

	@RequestMapping("/chapter5/author/del.html/{id}")
	public String delete(@PathVariable("id") int id) {
		repository.delete(id);
		return "redirect:/chapter5/author/list.html";
	}

	@RequestMapping(value = "/chapter5/author/dels.html", method = RequestMethod.POST)
	public String delete(@RequestParam("ids") List<Integer> list) {
		repository.delete(list);
		return "redirect:/chapter5/author/list.html";
	}
}
