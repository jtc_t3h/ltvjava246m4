package vn.t3h.chapter7.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;

import vn.t3h.chapter5.dao.CategoryRepository;

public class Bai1_CategoryInterceptor implements HandlerInterceptor{

	CategoryRepository repository = new CategoryRepository();

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		request.setAttribute("categories", repository.getCategories());
		return true;
	}

}
