package vn.t3h.chapter7.interceptor;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Bai1_BgHomeController {

	@RequestMapping("/chapter7/index.html")
	public String index() {
		return "chapter7.bai1.index";
	}
	
	
}
