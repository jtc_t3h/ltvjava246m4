package vn.t3h.chapter9.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import vn.t3h.chapter5.dao.Repository;
import vn.t3h.chapter6.utils.Helper;
import vn.t3h.chapter9.domain.Bai2_Member;
import vn.t3h.chapter9.domain.Bai2_Role;

public class Bai2_MemberRepository extends Repository {

	public int add(Bai2_Member obj) {
		obj.setId(Helper.randomLong());
		int a = jdbc.update(
				"INSERT INTO Member(MemberId, Username, Password, Email, Gender, Tel, Address) VALUES(?, ?, ?, ?, ?, ?, ?)",
				obj.getId(), obj.getUsername(), Helper.bcrypt(obj.getPassword()), obj.getEmail(), obj.isGender(),
				obj.getTel(), obj.getAddress());
		int b = jdbc.update("INSERT INTO MemberInRole(MemberId, RoleId) VALUES(?, ?)", obj.getId(), 1);
		return a + b;
	}

	public Bai2_Member logon(String username) {
		Bai2_Member member = jdbc.queryForObject("SELECT MemberId, Username, Password FROM Member WHERE Username = ?",
				new RowMapper<Bai2_Member>() {
					@Override
					public Bai2_Member mapRow(ResultSet rs, int numRow) throws SQLException {
						return new Bai2_Member(rs.getLong("MemberId"), rs.getString("UserName"),
								rs.getString("Password"));
					}
				}, username);
		member.setRoles(getRolesByMemberId(member.getId()));
		return member;
	}

	public List<Bai2_Role> getRolesByMemberId(long id) {
		return jdbc.query(
				"SELECT Role.RoleId, RoleName FROM Role INNER JOIN MemberInRole ON Role.RoleId = MemberInRole.RoleId WHERE MemberId = ?",
				new RowMapper<Bai2_Role>() {

					@Override
					public Bai2_Role mapRow(ResultSet rs, int arg1) throws SQLException {
						return new Bai2_Role(rs.getInt("RoleId"), rs.getString("RoleName"));
					}
				}, id);
	}
}
