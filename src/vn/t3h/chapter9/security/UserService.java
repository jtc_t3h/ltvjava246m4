package vn.t3h.chapter9.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import vn.t3h.chapter9.dao.Bai2_MemberRepository;
import vn.t3h.chapter9.domain.Bai2_Member;
import vn.t3h.chapter9.domain.Bai2_Role;

public class UserService implements UserDetailsService {

	Bai2_MemberRepository repository = new Bai2_MemberRepository();
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Bai2_Member obj = repository.logon(username);
		System.out.println(obj.getUsername());
		
		List<GrantedAuthority> authorities = new ArrayList<>();
		for (Bai2_Role role : obj.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		return new User(obj.getUsername(), obj.getPassword(), true, true, true, true, authorities);
	}
	
}
