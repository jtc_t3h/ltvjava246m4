package vn.t3h.chapter9;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Chapter9_HomeController {

	@RequestMapping("/chapter9/home/index.html")
	public String index() {
		return "chapter9.home.index";
	}
}
