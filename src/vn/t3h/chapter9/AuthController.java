package vn.t3h.chapter9;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.chapter9.dao.Bai2_MemberRepository;
import vn.t3h.chapter9.domain.Bai2_Member;

@Controller
public class AuthController {

	private Bai2_MemberRepository repository = new Bai2_MemberRepository();
	

	@RequestMapping("/chapter9/auth/bai2_register.html")
	public String register() {
		return "chapter9.auth.register";
	}
	
	@RequestMapping(value = "/chapter9/auth/bai2_register.html", method = RequestMethod.POST) 
	public String register(Bai2_Member obj) {
		repository.add(obj);
		return "redirect:/chapter9/auth/bai3_logon.html";
	}
	
	@RequestMapping("/chapter9/auth/bai3_logon.html")
	public String logOn() {
		return "chapter9.auth.logon";
	}
	
}
