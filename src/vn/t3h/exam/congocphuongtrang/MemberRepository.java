package vn.t3h.exam.congocphuongtrang;

import java.util.List;

import vn.t3h.chapter5.dao.Repository;
import vn.t3h.exam.congocphuongtrang.Member;
import vn.t3h.exam.congocphuongtrang.MemberMapper;

public class MemberRepository extends Repository{
	
	public List<Member> getMembers(){
		return jdbc.query("select * from member", new MemberMapper());
	}
	
	public int add(Member obj) {
		return jdbc.update("INSERT INTO member (MemberId, Username, Password, Email, Fullname, Gender) VALUES(?,?,?,?,?,?)", 
				obj.getId(), obj.getUsername(), obj.getPassword(), obj.getEmail(), obj.getFullname(), obj.getGender());
	}
	
	public Member getMember(int id){
		return jdbc.queryForObject("SELECT * FROM member WHERE MemberId = ?", new MemberMapper(), id);
	}
	
	public int edit(Member obj) {
		return jdbc.update("UPDATE Member SET Username=?, Password=?, Email=?, Fullname=?, Gender=? WHERE MemberId = ?", 
				obj.getUsername(), obj.getPassword(), obj.getEmail(), obj.getFullname(), obj.getGender(),obj.getId()); 
	}
	
	public int delete(int id) {
		return jdbc.update("DELETE FROM member WHERE MemberId = ?", id);
	}

}
