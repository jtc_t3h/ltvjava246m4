package vn.t3h.exam.congocphuongtrang;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.t3h.exam.congocphuongtrang.Member;
import vn.t3h.exam.congocphuongtrang.MemberRepository;

@RestController
public class CoNgocPhuongTrang_MemberController {

	private MemberRepository repository = new MemberRepository();

	// 1
	@GetMapping("/exam/api/congocphuongtrang/member")
	public List<Member> index() {
		return repository.getMembers();
	}

	// 2
	@GetMapping("/exam/api/congocphuongtrang/member/{id}")
	public Member detail(@PathVariable("id") int id) {
		return repository.getMember(id);
	}

	// 3
	@PostMapping("/exam/api/congocphuongtrang/member")
	public int post(Member obj) {
		return repository.add(obj);
	}

	// 4
	@PutMapping("/exam/api/congocphuongtrang/member/{id}")
	public int edit(Member obj, @PathVariable("id") int id) {
		//System.out.println(id);
		return repository.edit(obj);
	}

	// 5
	@DeleteMapping("/exam/api/congocphuongtrang/member/{id}")
	public int delete(@PathVariable("id") int id) {
		//System.out.println(id);
		
		return repository.delete(id);
	}
}