package vn.t3h.exam.congocphuongtrang;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MemberMapper implements RowMapper<Member> {

	@Override
	public Member mapRow(ResultSet ar, int arg1) throws SQLException {
		Member mem = new Member();
		mem.setId(ar.getLong(1));
		mem.setUsername(ar.getString(2));
		mem.setPassword(ar.getString(3));
		mem.setEmail(ar.getString(4));
		mem.setFullname(ar.getString(5));
		mem.setGender(ar.getInt(6));
		return mem;
	}
}
