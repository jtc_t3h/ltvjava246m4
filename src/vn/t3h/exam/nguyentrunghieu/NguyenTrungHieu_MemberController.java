package vn.t3h.exam.nguyentrunghieu;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NguyenTrungHieu_MemberController  {
	private MemberRepository repository = new MemberRepository();

	
	@GetMapping("/api/member")
	public List<Member> index() {
		return repository.getMembers();
	}

	
	@GetMapping("/api/member/{id}")
	public Member detail(@PathVariable("id") long id) {
		return repository.getMember(id);
	}

	
	@PostMapping("/api/member")
	public int post(Member obj) {
		return repository.add(obj);
	}

	
	@PutMapping("/api/member")
	public int edit(Member obj) {
		return repository.edit(obj);
	}

	
	@DeleteMapping("/api/member")
	public int delete(int id) {
		System.out.println(id + "===========");
		
		return repository.delete(id);
	}

}
