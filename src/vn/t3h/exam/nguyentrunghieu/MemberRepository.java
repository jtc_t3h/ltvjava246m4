package vn.t3h.exam.nguyentrunghieu;

import java.util.List;

public class MemberRepository extends Repository {
	public List<Member> getMembers() {
		return jdbc.query("select * from Member", new MemberMapper());
	}

	public int add(Member obj) {
		return jdbc.update("INSERT INTO Member (userName) VALUES(?)", obj.getUsername());
	}

	public int edit(Member obj) {
		return jdbc.update("UPDATE Member SET userName = ? WHERE MemberId = ?", obj.getUsername(), obj.getMemberId());
	}

	public Member getMember(long id) {
		return jdbc.queryForObject("SELECT * FROM Member WHERE MemberId = ?", new MemberMapper(), id);
	}

	public int delete(long id) {
		return jdbc.update("DELETE FROM Member WHERE MemberId = ?", id);
	}

	public int delete(List<Integer> list) {
		for (Integer id : list) {
			jdbc.update("DELETE FROM Member WHERE MemberId = ?", id);
		}
		return 1;
	}

}
