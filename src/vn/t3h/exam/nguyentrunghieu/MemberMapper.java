package vn.t3h.exam.nguyentrunghieu;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MemberMapper implements RowMapper<Member>{
	@Override
	public Member mapRow(ResultSet rs, int arg1) throws SQLException {
		Member member= new Member();
		member.setMemberId(rs.getLong(1));
		member.setFullname(rs.getString(2));
		member.setPassword(rs.getString(3));
		member.setEmail(rs.getString(4));
		member.setGender(rs.getByte(5));
		member.setAddedDate(rs.getDate(8));
		return  member;
	}

	

}
