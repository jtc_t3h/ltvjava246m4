package vn.t3h.exam.daovanthang;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DaoVanThan_MemberController {
	private MemberRepository repository=new MemberRepository();
	@GetMapping("/exam/api/daovanthang/member")
	public List<Member> getMembers(){
		return repository.getMembers();
	}
	@GetMapping("/exam/api/daovanthang/member/{id}")
	public Member getMember(@PathVariable("id") long id) {
		return repository.getMember(id);
	}
	@PostMapping("/exam/api/daovanthang/member")
	public int post(Member obj) {
		return repository.add(obj);
	}
	@PutMapping("/exam/api/daovanthang/member/{id}")
	public int edit(@PathVariable("id") long id) {
	return repository.edit(repository.getMember(id));
	}
	@DeleteMapping("/exam/api/daovanthang/member/{id}")
	public int delete(@PathVariable("id") long id) {
		int kq=repository.delete(id);
		if(kq==1) {
			System.out.println("THanh cong");
		}else {
			System.out.println("khong thanh cong");
		}
		return kq;
	}



}
