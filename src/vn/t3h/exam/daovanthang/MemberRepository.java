package vn.t3h.exam.daovanthang;

import java.util.List;

public class MemberRepository extends Repository{
	public List<Member> getMembers() {
		return jdbc.query("select * from member", new MemberMapping());
	}
	public Member getMember(long id) {
		
		return jdbc.queryForObject("SELECT * FROM member WHERE MemberId = ?", new MemberMapping(),id);
	}
	public int add(Member obj) {
		return jdbc.update("INSERT INTO member (Username,Password,Email,Tel,Address,AddedDate) VALUES(?,?,?,?,?,?)", obj.getUsername(),obj.getPassword(),obj.getEmail(),obj.getTel(),obj.getAddress(),obj.getAddedDate());
	}
	public int edit(Member obj) {
		return jdbc.update("UPDATE member SET (Username,Password,Email,Tel,Address,AddedDate) VALUES(?,?,?,?,?,?) WHERE MemberId = ?",obj.getUsername(),obj.getPassword(),obj.getEmail(),obj.getTel(),obj.getAddress(),obj.getAddedDate(),obj.getId());
	}
	public int delete(long id) {
		return jdbc.update("DELETE FROM member WHERE MemberId = ?", id);
	}






}
