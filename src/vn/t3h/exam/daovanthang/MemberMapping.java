package vn.t3h.exam.daovanthang;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MemberMapping implements RowMapper<Member>{

	@Override
	public Member mapRow(ResultSet rs, int arg1) throws SQLException {
		Member member=new Member();
		member.setId(rs.getLong(1));
		member.setUsername(rs.getString(2));
		member.setPassword(rs.getString(3));
		member.setEmail(rs.getString(4));
		member.setGender(rs.getInt(5));
		member.setTel(rs.getString(6));
		member.setAddress(rs.getString(7));
		member.setAddedDate(rs.getDate(8));
		member.setModifiedDate(rs.getDate(9));
		return member;
	}
	
	
	

}
