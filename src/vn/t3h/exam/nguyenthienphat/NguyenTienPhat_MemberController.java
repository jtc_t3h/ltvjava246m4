package vn.t3h.exam.nguyenthienphat;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("exam/nguyenthienphat/member")
public class NguyenTienPhat_MemberController {
	MemberRepository repository = new MemberRepository();

	@RequestMapping("index.html")
	public String index(Model model) {
		model.addAttribute("list", repository.getMembers());
		return "member.index";
	}
	//ADD:
	@RequestMapping("add.html")
	public String add() {
	return "member.add";
	}
	@RequestMapping( value = "add.html", method = RequestMethod.POST)
	public String add(Member obj) {
	repository.add(obj);
	return "redirect:/exam/nguyenthienphat/member/index.html";
	}
	//Edit:
	@RequestMapping("edit.html/{id}")
	public String edit(Model model, @PathVariable("id") int id) {
	model.addAttribute("o", repository.getMembers(id));
	return "member.edit";
	}
	@RequestMapping(value = "edit.html/{id}", method=RequestMethod.POST)
	public String edit(Model model, Member obj, @PathVariable("id") int id) {
	repository.edit(obj);
	return "redirect:/exam/nguyenthienphat/member/index.html";
	}
}
