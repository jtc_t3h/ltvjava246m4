package vn.t3h.exam.nguyenthienphat;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class MemberApiController {
	MemberRepository repository = new MemberRepository();

	@GetMapping("exam/api/nguyenthienphat/member")
	public List<Member> index() {
		return repository.getMembers();
	}

//	@GetMapping("exam/api/nguyenthienphat/member/{id}")
//	public List<Member> detail(@PathVariable("id") int id) {
//		return repository.getMembers(id);
//	}

	@PostMapping("exam/api/nguyenthienphat/member")
	public int post(Member obj) {
		return repository.add(obj);
	}

	@PutMapping("exam/api/nguyenthienphat/member")
	public int edit(Member obj) {
		return repository.edit(obj);
	}


}
