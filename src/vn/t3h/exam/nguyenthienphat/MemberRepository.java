package vn.t3h.exam.nguyenthienphat;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.RowMapper;







public class MemberRepository extends Repository {
	public List<Member> getMembers() {
		return jdbc.query("SELECT * FROM Member", new RowMapper<Member>() {
			@Override
			public Member mapRow(ResultSet rs, int numRow) throws SQLException {
				return new Member(
						rs.getLong("MemberId"),
						rs.getString("Username"),
						rs.getString("Password"),
						rs.getString("Email"),
						 rs.getByte("Gender"),
						rs.getInt("Tel"),
						rs.getString("Address"));
			}
		});
	}
	
	// ADD:
		public int add(Member obj) {
			return jdbc.update("INSERT INTO Member(Username, Password, Email, Gender, Tel, Address) VALUES(?, ?, ?, ?, ?, ?)",
					obj.getUsername(), 
					obj.getId(),
					obj.getPassword(),
					obj.getEmail(),
					obj.getGender(),
					obj.getTel(),
					obj.getAddress()
					);
		}
		//Edit:
		// Edit:
		public int edit(Member obj) {
			return jdbc.update("UPDATE Member SET Username = ? WHERE MemberId = ?", obj.getUsername(), obj.getId());
		}

		public Member getMembers(int id) {
			return jdbc.queryForObject("SELECT * FROM Author WHERE AuthorId = ?", new RowMapper<Member>() {
				@Override
				public Member mapRow(ResultSet rs, int numRow) throws SQLException {
					return new Member(
							rs.getLong("MemberId"),
							rs.getString("Username"),
							rs.getString("Password"),
							rs.getString("Email"),
							 rs.getByte("Gender"),
							rs.getInt("Tel"),
							rs.getString("Address"));
				}
			}, id);
		}

}
//private long id;
//private String username;
//private String password;
//private String email;
//private String fullname;
//private byte gender;
//private Date addedDate;

