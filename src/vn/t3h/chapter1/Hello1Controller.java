package vn.t3h.chapter1;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author phaolo
 *
 */
@Controller
public class Hello1Controller {

	@RequestMapping("chapter1/hello.html")
	public String hello() {
		return "chapter1/hello";
	}
}
