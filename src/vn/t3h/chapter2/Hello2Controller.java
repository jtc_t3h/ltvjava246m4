package vn.t3h.chapter2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Hello2Controller {

	@RequestMapping(value = "chapter2/welcome/{name}")
	public String welcome(@PathVariable(value="name", required=true) String name, HttpServletRequest req, HttpServletResponse res) {
		req.setAttribute("name", name);
		return "chapter2/welcome";
	}
}
