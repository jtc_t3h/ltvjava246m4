package vn.t3h.chapter2;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Bai1_SumController {

	@RequestMapping("chapter2/bai1_sum.html")
	public String index() {
		
		return "chapter2/bai1_sum";
	}
	
	@RequestMapping(value="chapter2/bai1_sum.html", method=RequestMethod.POST)
	public ModelAndView calculator(@RequestParam("a") int a, @RequestParam("b") int b) {
		ModelAndView mav = new ModelAndView("chapter2/bai1_sum");
		mav.addObject("a", a);
		mav.addObject("b", b);
		mav.addObject("result", a + b);
		return mav;
	}
}
