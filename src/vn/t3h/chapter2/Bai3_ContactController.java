package vn.t3h.chapter2;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.chapter2.domain.Contact;

@Controller
public class Bai3_ContactController {

	@RequestMapping("chapter2/bai3_contact.html")
	public String contact() {
		return "chapter2/bai3_contact";
	}

	@RequestMapping(value = "chapter2/bai3_contact.html", method = RequestMethod.POST)
	public String contact(Model model, Contact obj) {
		model.addAttribute("o", obj);
		return "chapter2/bai3_contact";
	}
}
