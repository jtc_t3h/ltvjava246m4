package vn.t3h.chapter4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.chapter4.domain.MailInfo;

@Controller
public class Bai3_SendEmailController {

	@Autowired
	private JavaMailSender mailSender;

	@RequestMapping("chapter4/bai3_sendmail.html")
	public String index() {
		return "chapter4.bai3_send_mail";
	}

	@RequestMapping(value = "chapter4/bai3_sendmail.html", method = RequestMethod.POST)
	public String index(@ModelAttribute("obj") MailInfo obj) {
				
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(obj.getEmail());
		message.setSubject(obj.getSubject());
		message.setText(obj.getContent());
		
		mailSender.send(message);
		
		return "chapter4.bai3_send_mail";
	}
}
