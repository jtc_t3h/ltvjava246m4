package vn.t3h.chapter6;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import vn.t3h.chapter6.dao.InvoiceRepository;
import vn.t3h.chapter6.domain.Invoice;

@Controller
@RequestMapping("/chapter6/order/")
public class Chapter6_OrderController {

	InvoiceRepository repository = new InvoiceRepository();
	
	@RequestMapping("list.html")
	public String index() {
		return "chapter6.order.list";
	}
	@RequestMapping("detail.html/{id}")
	public String detail(Model model, @PathVariable("id") String id) {
		
		Invoice obj = repository.getInvoice(id);
		model.addAttribute("o", obj);
		model.addAttribute("title", "Order Detail");
		return "chapter6.order.detail";
	}
}
