package vn.t3h.chapter6;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.chapter5.dao.CategoryRepository;
import vn.t3h.chapter5.domain.Category;

@Controller
@RequestMapping("/chapter6/category/")
public class Chapter6_CategoryController {

	private CategoryRepository repository = new CategoryRepository();

	@RequestMapping("list.html")
	public String list(Model model) {
		model.addAttribute("list", repository.getCategories());
		return "chapter6.category.list";
	}

	@RequestMapping("add.html")
	public String add(Model model) {
		List<Category> list = repository.getCategories();
		Map<Integer, String> map = new HashMap();
		for (Category item : list) {
			map.put(item.getId(), item.getName());
		}
		model.addAttribute("map", map);
		model.addAttribute("category", new Category());

		return "chapter6.category.add";
	}

	@RequestMapping(value = "add.html", method = RequestMethod.POST)
	public String add(Category obj) {
		repository.add(obj);
		
		return "chapter6.category.add";
//		return "redirect:/chapter6/category/list.html";
	}

	@RequestMapping("edit.html/{id}")
	public String edit(Model model, @PathVariable("id") int id) {
		List<Category> list = repository.getCategories();
		Map<Integer, String> map = new HashMap<>();
		for (Category item : list) {
			map.put(item.getId(), item.getName());
		}
		model.addAttribute("map", map);
		model.addAttribute("obj", repository.getCategory(id));
		return "chapter6.category.edit";
	}

	@RequestMapping(value = "edit.html/{id}", method = RequestMethod.POST)
	public String edit(Model model, Category obj, @PathVariable("id") int id) {
		repository.edit(obj);
		return "redirect:/chapter6/category/list.html";
	}
}
