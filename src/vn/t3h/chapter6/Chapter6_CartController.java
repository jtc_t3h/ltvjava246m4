package vn.t3h.chapter6;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.t3h.chapter6.dao.CartRepository;
import vn.t3h.chapter6.dao.InvoiceRepository;
import vn.t3h.chapter6.domain.Cart;
import vn.t3h.chapter6.domain.Invoice;
import vn.t3h.chapter6.utils.Helper;

@Controller
@RequestMapping("/chapter6/cart/")
public class Chapter6_CartController {
	private CartRepository repository = new CartRepository();
	private InvoiceRepository invoiceRepository = new InvoiceRepository();

	@RequestMapping("list.html")
	public String index(Model model, @CookieValue("cart") String id) {
		model.addAttribute("title", "Your Cart");
		model.addAttribute("list", repository.getCarts(id));
		return "chapter6.cart.list";
	}

	@RequestMapping(value = "add.html", method = RequestMethod.POST)
	public String add(Model model, Cart obj, HttpServletRequest request, HttpServletResponse response) {
		String id = null;
		for (Cookie cookie : request.getCookies()) {
			if (cookie.getName().equals("cart")) {
				id = cookie.getValue();
			}
		}
		if (id == null) {
			id = Helper.randomString(32);
			Cookie cookie = new Cookie("cart", id);
			cookie.setPath(request.getServletContext().getContextPath());
			cookie.setMaxAge(30 * 24 * 3600);
			response.addCookie(cookie);
		}
		obj.setId(id);
		repository.add(obj);
		return "redirect:/chapter6/cart/list.html";
	}

	@RequestMapping("checkout.html")
	public String checkout(Model model, @CookieValue("cart") String id) {
		model.addAttribute("title", "Check Out");
		model.addAttribute("list", repository.getCarts(id));
		return "chapter6.cart.checkout";
	}

	@RequestMapping(value = "checkout.html", method = RequestMethod.POST)
	public String checkout(Model model, Invoice obj, @CookieValue("cart") String id) {
		obj.setId(id);
		invoiceRepository.add(obj);
		return "redirect:/chapter6/order/detail.html/" + obj.getId();
	}

}
