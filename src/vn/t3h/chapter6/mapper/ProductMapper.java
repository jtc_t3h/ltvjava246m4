package vn.t3h.chapter6.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import vn.t3h.chapter6.domain.Product;

public class ProductMapper implements RowMapper<Product>{

	@Override
	public Product mapRow(ResultSet rs, int arg1) throws SQLException {
		Product product = new Product();
		
		product.setProductId(rs.getInt("productId"));
		product.setCategoryId(rs.getInt("categoryId"));
		product.setPublisherId(rs.getInt("publisherId"));
		product.setAuthorId(rs.getInt("authorId"));
		product.setIsbn(rs.getString("isbn"));
		product.setTitle(rs.getString("title"));
		product.setPages(rs.getInt("pages"));
		product.setYears(rs.getInt("year"));
		product.setWeight(rs.getString("weight"));
		product.setSize(rs.getString("size"));
		product.setDescription(rs.getString("description"));
		product.setContent(rs.getString("content"));
		product.setImageUrl(rs.getString("imageUrl"));
		product.setPrice(rs.getInt("price"));
		
		return product;
	}

}
