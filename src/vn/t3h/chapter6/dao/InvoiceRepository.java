package vn.t3h.chapter6.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import vn.t3h.chapter5.dao.Repository;
import vn.t3h.chapter6.domain.Invoice;
import vn.t3h.chapter6.domain.InvoiceDetail;

public class InvoiceRepository extends Repository {
	public int add(Invoice obj) {
		return jdbc.update("CALL AddInvoice(?, ?, ?, ?, ?)", obj.getId(), obj.getMemberId(), obj.getEmail(),
				obj.getTel(), obj.getAddress());
	}
	
	public Invoice getInvoice(String id) {
		return jdbc.queryForObject("SELECT InvoiceId, MemberId, Tel, Address, Email, Invoice.StatusId,StatusName, AddedDate FROM Invoice INNER JOIN Status ON Invoice.StatusId = Status.StatusId WHERE InvoiceId = ?", new RowMapper<Invoice>() {
			@Override
			public Invoice mapRow(ResultSet rs, int arg1) throws SQLException {
				Invoice obj = new Invoice( rs.getString("InvoiceId"), rs.getLong("MemberId"), rs.getString("Tel"), rs.getString("Address"), rs.getString("Email"), rs.getByte("StatusId"), rs.getString("StatusName"), rs.getDate("AddedDate"));
				obj.setDetails(getInvoiceDetails(id));
				return obj;
			}
		}, id);
	}
	public List<InvoiceDetail> getInvoiceDetails(String id){
		return jdbc.query("SELECT * FROM InvoiceDetail INNER JOIN Product ON InvoiceDetail.ProductId = Product.ProductId WHERE InvoiceId = ?", new RowMapper<InvoiceDetail>() {

			@Override
			public InvoiceDetail mapRow(ResultSet rs, int arg1) throws SQLException {
				
				return new InvoiceDetail(rs.getString("InvoiceId"), rs.getInt("ProductId"), rs.getShort("Quantity"), rs.getInt("Price"), rs.getString("Title"), rs.getString("ImageUrl"));
			}
			
		}, id);
	}
}
