package vn.t3h.chapter6;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import vn.t3h.chapter6.dao.ProductRepository;
import vn.t3h.chapter6.domain.Product;

@Controller
@RequestMapping("/chapter6/product/")
public class Chapter6_ProductController {

	ProductRepository repository = new ProductRepository();
	private static int size = 6;

	@RequestMapping(value = { "list.html", "list.html/{p}" })
	public String index(Model model, @PathVariable(value = "p", required = false) Integer p) {
		if (p == null) {
			p = 1;
		}

		model.addAttribute("title", "Mini Shop");
		model.addAttribute("n", (int) Math.ceil(repository.count() / (double) size));
		model.addAttribute("list", repository.getProducts((p - 1) * size, size));

		return "chapter6.product.list";
	}

	@RequestMapping("detail.html/{id}")
	public String detail(Model model, @PathVariable("id") int id) {
		
		Product o = repository.getProduct(id);
		model.addAttribute("title", o.getTitle());
		model.addAttribute("o", o);
		
		return "chapter6.product.detail";
	}
	
	@RequestMapping("search.html")
	public String search(Model model, @RequestParam(name = "q", required = false) String q) { 
		
		model.addAttribute("title", "Result for " + q); 
		model.addAttribute("list", repository.search(q));
	
		return "chapter6.product.search";
	}
}
