package vn.t3h.chapter8.api;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.t3h.chapter5.dao.Bai2_AuthorRepository;
import vn.t3h.chapter5.domain.Bai2_Author;

@RestController
public class AuthorApiController {

	private Bai2_AuthorRepository repository = new Bai2_AuthorRepository();

	// Bai 1
	@GetMapping("/chapter8/api/author")
	public List<Bai2_Author> index() {
		return repository.getAuthors();
	}

	// Bai 2
	@GetMapping("/chapter8/api/author/{id}")
	public Bai2_Author detail(@PathVariable("id") int id) {
		return repository.getAuthor(id);
	}

	// Bai 3
	@PostMapping("/chapter8/api/author")
	public int post(Bai2_Author obj) {
		return repository.add(obj);
	}

	// Bai 4
	@PutMapping("/chapter8/api/author")
	public int edit(Bai2_Author obj) {
		System.out.println(obj.getId());
		System.out.println(obj.getName());
		return repository.edit(obj);
	}

	// Bai 5
	@DeleteMapping("/chapter8/api/author")
	public int delete(int id) {
		System.out.println(id + "===========");
		
		return repository.delete(id);
	}
}
